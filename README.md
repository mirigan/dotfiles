# Mirigan's dotfiles

Dotfiles and applications for my Linux/Unix workstations
Uses GNU Stow – https://www.gnu.org/software/stow/

```bash
stow . # to install

```

## Fonts & Theme

Use NerdFonts <a href="https://www.nerdfonts.com/">nerdfonts.com</a> to install them.

- [FiraCode](https://github.com/tonsky/FiraCode)
- [JetBrainsMono](https://www.jetbrains.com/lp/mono/)

- [Catppuccin](https://github.com/catppuccin) for Firefox, IDE, terminal ...

## Terminal

For terminal emulator I use <a href="https://sw.kovidgoyal.net/kitty/">Kitty</a>
(I would like to also try <a href="https://ghostty.org/">Ghostty</a>)

<a href="https://ohmyz.sh/#install">Oh-my-zsh install link</a>

### OhMyZsh theme and plugins

```bash
ZSH_THEME="robbyrussell"

COMPLETION_WAITING_DOTS="true"

plugins=(git rbenv rails bundler zsh-autosuggestions zsh-syntax-highlighting docker)
```

## Dev Tech

### IDE

#### VSCode

_Theme used : Catpuccin_
SyncSettings with GitHub account

### Other Dev Applications

- [lazygit](https://github.com/jesseduffield/lazygit) -> A simple terminal UI for git commands
- [exa](https://the.exa.website/) -> A modern replacement for ls
- [bat](https://github.com/sharkdp/bat) -> cat clone with syntax highlighting and Git integration


- [nvm](https://github.com/nvm-sh/nvm) -> nodeJs versions manager
- [rbenv](https://github.com/rbenv/rbenv) -> Ruby versions manager
- [gitmoji](https://gitmoji.dev/) -> To generate fancy git message
- [GitKraken](https://www.gitkraken.com/) -> Git GUI
- [DBeaver](https://dbeaver.io/) -> Free and OpenSource database administrators

### Usefull but not used today

- [Desk](https://github.com/jamesob/desk) -> Workspace manager for the shell

## Common Applications

- Spotify _Music_
- [FireFox Dev](https://www.mozilla.org/fr/firefox/developer/)
- [Chromium](https://www.chromium.org/)
- [Element](https://element.io/) _Perso Chat_

## Usefull Links

- [endoflife.date](https://endoflife.date/) to track end of support for a lot of things like OS, framworks and so on

## Socials links

<p align="center">
    <a href="https://twitter.com/Mirigan_"><img src="https://img.shields.io/badge/twitter-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white" /></a>
    <a href="https://github.com/Mirigan"><img src="https://img.shields.io/badge/github-333?style=for-the-badge&logo=github&logoColor=white" /></a>
    <a href="https://gitlab.com/mirigan"><img src="https://img.shields.io/badge/Gitlab-554488?style=for-the-badge&logo=Gitlab&logoColor=white" /></a>
    <a href="https://mastodon.top/@mirigan"><img src="https://img.shields.io/badge/mastodon-2b90d9?style=for-the-badge&logo=mastodon&logoColor=white" /></a>
</p>
